###Python Brightness Control for XRandR 0.1###

Set the brightness level of all connected screens using X11.

Accepts one parameter, ranging from 0.1 (dim) to 1.0 (normal)


This will not affect backlight levels and is temporary until the next reboot.

Example: ./pybright.py 0.8

